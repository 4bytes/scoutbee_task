.PHONY: all run_test clean fetch_drivers

DRIVERSDIR="drivers"

all: fetch_drivers run_test

run_test:
	@echo ">>> Running the test"
	@ pytest -k 'StackOverFlow' --url 'https://stackoverflow.com' -v -s --log-cli-level=info

fetch_drivers:
	@echo ">>> Fetching the drivers"
	@./scripts/fetch_drivers.bash $(DRIVERSDIR)

clean:
	@echo ">>> Cleaning up..."
	@find -name '__pycache__' -type d -prune -exec rm -r {} \;
