# -*- coding: utf-8 -*-

import pytest

from pathlib import Path

from selenium.webdriver import (Firefox, Chrome)
from urllib.parse import (urlparse, urlunparse)

def pytest_addoption(parser):
        parser.addoption(
            '--url',
            action='store',
            default='localhost',
            help='url',
        )

@pytest.fixture(params=['chrome', 'firefox'], scope='class')
def driver_init(request):
    # TODO: fix driver binary path using Pathlib
    if request.param == 'chrome':
        web_driver = Chrome(executable_path='drivers/chromedriver')
    if request.param == 'firefox':
        web_driver = Firefox(executable_path='drivers/geckodriver')

    url = request.config.option.url

    if not urlparse(url).scheme:
        url = ('http', url, '', '', '', '')
        url = urlunparse(url)

    web_driver.maximize_window()
    web_driver.get(url)

    request.cls.driver = web_driver
    yield web_driver

    web_driver.close()

@pytest.mark.hookwrapper
def pytest_runtest_makereport(item, call):
    pytest_html = item.config.pluginmanager.getplugin('html')
    outcome = yield
    report = outcome.get_result()
    extra = getattr(report, 'extra', [])
    if report.when == 'call':
        # extra.append(pytest_html.extras.image('foo.png'))
        xfail = hasattr(report, 'wasxfail')
        if (report.skipped and xfail) or (report.failed and not xfail):
            extra.append(pytest_html.extras.html(
                    '<div>Additional HTML</div>'))
        report.extra = extra
