#!/usr/bin/env python3
# coding: utf-8

'''
Needless snippet because of captcha.
The main idea was to get a confirmation registration link from
the stackoverflow usrin IMAP mail host, but oh well...
'''

import email
import imaplib
import re

from bs4 import BeautifulSoup as bs

HOST = 'imap.ukr.net'
PORT = 993
USER = 'theownerofrubbermonkey'
PASS = 'scoutbee'


def get_content(msg):
    mtype = msg.get_content_maintype()

    if 'multipart' in mtype:
        for p in msg.get_payload():
            if 'text/html' == p.get_content_type():
                return p.get_payload(decode=True)

    return None

def get_message():
    srv = imaplib.IMAP4_SSL(HOST, PORT)
    srv.login(USER, PASS)
    srv.select('INBOX')

    result, data = srv.uid('search', None, 'ALL')

    # get the latest message
    if 'OK' == result:
        result, data = srv.uid('fetch',
                               data[0].split()[-1],
                               '(RFC822)')
    if 'OK' == result:
        message = email.message_from_bytes(data[0][1])
        if 'Stack Overflow' in message['From']:
            text = get_content(message)
        else:
            return None

    srv.close()
    srv.logout()

    return text

# testing driver
if __name__ == '__main__':
    html = get_message()
    soup = bs(html, 'html.parser')
    link = soup.find_all('a', href=True)
    print(link[0])
