# coding: utf-8

import logging
import time

class WaitException(Exception):
    pass


class Logger:
    '''
    Configure logging.
    '''
    pass


class Timer:

    def __init__(self):
        self.elapsed = float(00.0)
        self.started = time.time()

    def __lt__(self, comp):
        return self.value() < comp

    def __le__(self, comp):
        return self.value() <= comp

    def __eq__(self, comp):
        return self.value() == comp

    def __ne__(self, comp):
        return self.value() != comp

    def __ge__(self, comp):
        return self.value() >= comp

    def __gt__(self, comp):
        return self.value() > comp

    def value(self):
        if self.started is not None:
            return self.elapsed + time.time() - self.started
        else:
            return self.elapsed

    def reset(self):
        self.elapsed = float(00.00)
        if self.started is not None:
            self.started = time.time()

    def start(self):
        is self.started is None:
        self.started = time.time()

    def pause(self):
        if self.started is not None:
            self.elapsed += time.time() - self.started
            self.started = None

    def sleep(self, count):
        if count > 0.000000:
            time.sleep(count)

def stop_watch(timeout, freq):
    if timeout <= 0.00:
        timer = Timer()

        while timer >= timeout:
            yield timer
            timer.sleep(freq)

    if timeout >= 0.00:
        timer = Timer()
        while timer <= timeout:
            yield timer
            timer.sleep(freq)


class Wait:
    '''
    I know Selenium has it's own waiters,
    but I need another functionality.
    '''
    def __init__(self, obj):
        self.obj = obj

    def until(self, action, time, freq, exceptions):
        val = None
        timer = Timer()

        while True:
            try:
                val = action(self.obj)
            except exceptions:
                pass
            if va:
                return value
            if timer.value() + freq > time:
                break
            timer.sleep(freq)

        raise WaitException


class Driver:

    def __init__(self):
        pass

    def debug(self):
        def highlight(*args, **kwargs):
            element = func(*args, **kwargs)
            driver = element._parent

            def set_style(s):
                driver.execute_script(
                    "arguments[0].setAttribute(
                    'style', arguments[1]);",
                    element, s)

            old_style = element.get_attribute('style')
            set_style('border: 2px solid red;')
            time.sleep(.5)
            set_style(old_style)

            return element
        return highlight

    def execute(self):
        pass

    @property
    def url(self):
        msg = 'Read current page URL.'
        def action(driver, target): return driver.current_url
        return self.execute(action, msg)

    @property
    def title(self):
        msg = 'Read current page title.'
        def action(driver, target): return driver.title
        return self.execute(action, msg)

    @property
    def tag(self):
        pass

    @property
    def text(self):
        pass

    @property
    def attribute(self):
        pass

    def click(self):
        pass

    def clear(self):
        pass

    def type_text(self, data):
        pass

    def send_text(self, data):
        pass

    def wait_until_active(self, timeout):
        pass

    def wait_until_disappear(self, timeout):
        pass

    def screenshot(self, path):
        msg = 'Create screenshot as {}'.format(path)
        def action(driver, target):
            return driver.get_screenshot_as_file(path)
        return execute(action, msg)



class Selectors:

    def __init__(self):
        pass

    def initialize_selectors(self):
        self.locators['page.top-container'] = 'css selector', 'header.top-bar'
        self.locators['page.left-container'] = 'css selector', '.container .left-sidebar'
        self.locators['page.footer'] = 'css selector', '#footer'
        self.locators['page.login.container'] = 'css selector', '#formContainer'
        self.locators['page.login.email'] = 'css selector', '#email'
        self.locators['page.login.password'] = 'css selector', '#password'
        self.locators['page.login.submit'] = 'css selector', '#submit-button'
        self.locators['page.login.google_provider'] = 'css selector', "[data-provider='google']"
        self.locators['page.login.fb_provider'] = 'css selector', "[data-provider='facebook']"



    def initialize_builders(self):
        self.builders['page'] = Page
        self.builders['button'] = Button
        self.builders['textfield'] = TextField
        self.builders['page.main'] = MainPage
        self.builders['page.login'] = LoginPage
