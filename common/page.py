# codgin: utf-8


class Page(Driver):

    def __init__(self):
        self.log.info(f'Page class')


class Button(Driver):
    pass


class TextField(Driver):

    def read_only(self):
        pass

    def get_text(self):
        pass

    def append_text(self):
        pass

    def change_text(self):
        pass

    def verify_text(self):
        pass


class MainPage(Page):

    def __init__(self):
        super().__init__()
        self.log.info(f'Page class')

    # left side navigation container
    def click_questions_link(self):
        pass

    def click_tags_link(self):
        pass

    def click_users_link(self):
        pass

    def click_jobs_link(self):
        pass

    # top side container
    def main_page(self):
        pass

    def click_products_link(self):
        pass

    def click_customers_link(self):
        pass

    def click_use_cases_link(self):
        pass

    def search(self):
        pass

    def get_content(self):
        pass

    def extract_content_headers(self):
        pass

    def extract_content(self):
        '''make matrix'''
        pass


class LoginPage(Page):

    def wait_until_loaded(self):
        pass

    def login(self, username, password):
        pass

    def get_data_provider(self, provider):
        pass
