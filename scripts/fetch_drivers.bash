#!/usr/bin/env bash

set -o errexit
set -o nounset

readonly ME="${0##*/}"
readonly ARCH="linux64"

# firefox
readonly FV="v0.24.0"
readonly FN="geckodriver-${FV}-${ARCH}.tar.gz"
readonly FIREFOXDL="https://github.com/mozilla/geckodriver/releases/download/${FV}/${FN}"

# chrome
# readonly CV="74.0.3729.6"
readonly CV="73.0.3683.68"
readonly CN="chromedriver_${ARCH}.zip"
readonly CHROMEDL="https://chromedriver.storage.googleapis.com/${CV}/${CN}"

die() {
    echo "${ME}: ERROR: $@"
    exit 1
}

info() {
    echo "${ME}: INFO: $@"
}

get_fetcher() {
    for f in "wget" "curl"; do
	[[ $(type -p "$f") ]] && fetcher=$(type -p "$f") || continue
    done

    [[ -z "${fetcher}" ]] && die "wget or curl must be installed."

    echo "${fetcher}"
}

unpack() {
    local source="$1"; shift
    local target="$1"; shift

    [[ ! -d "${target}" ]] && mkdir "${target}"
    pushd "${target}" &> /dev/null

    for f in "${source}"/*; do
	case "${f##*/}" in
	    "${FN}")
		tar xzf "$f"
		;;
	    "${CN}")
		unzip "$f"
		;;
	    *)
		die "Wrong file."
		;;
	esac
    done
    popd &> /dev/null
}

main() {

    { (( "${#}" == 0 )) || [[ -z "$@" ]] ;} &&
        die "The directory must be specified."

    local unpackdir="$1"; shift
    [[ -f "${unpackdir}/${FN%%-*}" && \
           -f "${unpackdir}/${CN%%_*}" ]] &&
	{ info "The drivers exist." ; exit ;}

    local tmpdir=$(mktemp -d)
    local cmd="$(get_fetcher)"

    [[ "${cmd//*\/}" == "curl" ]] && cmd+=" -OL"

    pushd "${tmpdir}" &> /dev/null
    info "Downloading drivers to '${tmpdir}'..."
    for d in "${FIREFOXDL}" "${CHROMEDL}"; do
	eval "${cmd} ${d}" &> /dev/null \
            || die "Unable to download the driver."
    done
    popd &> /dev/null

    unpack "${tmpdir}" "${unpackdir}"

    info "Remove temporary files"
    rm -rf "${tmpdir}"
}

main "$@"
